////////////////////////////////////////////////////////////////////
// handle server requests
////////////////////////////////////////////////////////////////////

#ifndef _IOTERS_WEBSERVER_H_
#define _IOTERS_WEBSERVER_H_

#include <ESP8266WebServer.h>
#include "Settings.h"
#include <ArduinoJson.h>
#include "OTAUpdate.h"
#include "Misc.h"
#include "www.h"

#define readParam(param_name, param){ if (server.hasArg(param_name)){  server.arg(param_name).toCharArray(param,sizeof(param)); } }

ESP8266WebServer server(80); // Server on port 80

void handleRoot() {
    // last_page_load = millis();
  DPRINTLN("Webserver root");
    String s = MAIN_page; //Read HTML contents
    server.send(200, "text/html", s); //Send web page
}

void handleInfo() {

    StaticJsonBuffer<200> jsonBuffer;
    String json_string = "";
    JsonObject& root    = jsonBuffer.createObject();

    root["chipid"]        = ESP.getChipId();;
    root["flashid"]       = ESP.getFlashChipId();
    root["flashmode"]     = ESP.getFlashChipMode();
    root["flashsize"]     = ESP.getFlashChipSize();
    root["flashrealsize"] = ESP.getFlashChipRealSize();
    root["flashspeed"]    = ESP.getFlashChipSpeed();
    root["mac"]           = WiFi.macAddress();
    root["hostname"]      = WiFi.hostname();
    root["sketchsize"]      = ESP.getSketchSize();
    root["freesketchspace"] = ESP.getFreeSketchSpace();
    root["coreversion"]     = ESP.getCoreVersion();

    
    root.printTo(json_string);
    server.send(200, "text/html", json_string); //Send web page
}

void handleStatus() {
    
    StaticJsonBuffer<800> jsonBuffer;
    String json_string = "";
    JsonObject& root    = jsonBuffer.createObject();
       
    root["heap"]            = ESP.getFreeHeap();
    root["ssi"]             = WiFi.RSSI();
    root["easy_name"]       = SETTINGS.easy_name;
    root["update_url"]      = SETTINGS.update_url;
    root["data_url"]        = SETTINGS.data_url;
    root["current_value"]   = current_value;
    root["current_color"]   = current_color;
    root["lastresetreason"] = myResetInfo->reason;
    root["firmware_version"] = firmware_version;

    root.printTo(json_string);    
    server.send(200, "text/html", json_string); //Send web page
}


void handleSave() {
 
  readParam("easy_name", SETTINGS.easy_name);
  readParam("update_url", SETTINGS.update_url);
  readParam("data_url", SETTINGS.data_url);
  
  DPRINTLN("Save params");
  SettingsSave();

  server.send(200, "text/html", "1"); //Send web page
}

void handlefirmwareUpdate() {
  server.send(200, "text/html", "1"); //Send web page
  delay(100);
  checkAutoUpdate();
}

void handleDataUpdate() {
  server.send(200, "text/html", "1"); //Send web page
  delay(100);
  current_value = WebClientReadData();
  current_color = RGBSet(current_value);
}

void handleTestValue() {
  server.send(200, "text/html", "1"); //Send web page
  delay(100);
  char value[6];
  readParam("test_value",value);
  DPRINT("Test value: ");
  DPRINT(value);
  if(atoi(value) == 0){
      DPRINT("Test all colors");
      RGBTest();
  }else{
    current_value = atoi(value);
    DPRINT(" Current value: ");
    DPRINTLN(current_value);
    current_color = RGBSet(current_value);
  }
}

char nibble_to_hex(uint8_t nibble) {  // convert a 4-bit nibble to a hexadecimal character
  nibble &= 0xF;
  return nibble > 9 ? nibble - 10 + 'A' : nibble + '0';
}

void byte_to_str(char* buff, uint8_t val) {  // convert an 8-bit byte to a string of 2 hexadecimal characters
  buff[0] = nibble_to_hex(val >> 4);
  buff[1] = nibble_to_hex(val);
}



void handleTestColor() {
  server.send(200, "text/html", "1"); //Send web page
  delay(100);
  char value_red[6];
  char value_green[6];
  char value_blue[6];
  
  readParam("red",value_red);
  readParam("green",value_green);
  readParam("blue",value_blue);
  
  RGBSetColor(atoi(value_red),atoi(value_green),atoi(value_blue));

  char rgbTxt[6];
  //rgbTxt[0] = '#';
  byte_to_str(&rgbTxt[0], atoi(value_red));
  byte_to_str(&rgbTxt[2], atoi(value_green));
  byte_to_str(&rgbTxt[4], atoi(value_blue));
  current_color = rgbTxt;
  DPRINTLN(rgbTxt);
  
}

void handleRestoreDefaults () {
  server.send(200, "text/html", "1"); //Send web page
  SettingsReset();
  restartESP();
}

void handleClient(){
    server.handleClient();
}


void WebServerInit(){
  DPRINTLN("Webserver start");
    ////////////////////////////////////////////////////
  server.on("/",                 handleRoot);               // Which routine to handle at root location
  server.on("/info",             handleInfo);               // Which routine to handle at info request
  server.on("/status",           handleStatus);             // Which routine to handle at status request
  server.on("/save",             handleSave);               // Which routine to handle at save request
  server.on("/restore_defaults", handleRestoreDefaults);    // Which routine to handle at restoreDefaults
  server.on("/firmware_update",  handlefirmwareUpdate);     // Which routine to handle at firmware_update
  server.on("/data_update",      handleDataUpdate);         // Which routine to handle at firmware_update
  server.on("/test_value",       handleTestValue); 
  server.on("/test_color",       handleTestColor);          
  server.begin();                     // Start server
  ////////////////////////////////////////////////////
}

#endif