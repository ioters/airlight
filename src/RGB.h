

#define RGB_RED 3
#define RGB_GREEN 0
#define RGB_BLUE 2


void RGBSetColor(int red, int green, int blue){
	
	DPRINT("Color set to:");
	DPRINT(red);
	DPRINT(",");
	DPRINT(green);
	DPRINT(",");
	DPRINTLN(blue);
	
	#ifndef DEBUG
		analogWrite(RGB_RED, 1023 - red*red/64);
		analogWrite(RGB_GREEN, 1023 - green*green/64);
		analogWrite(RGB_BLUE, 1023 - blue*blue/64);
	#endif	

}

void RGBFade(int pin) {

	for (int u = 0; u < 1024; u++) {
		analogWrite(pin, 1023 - u);
		delay(1);
	}
	for (int u = 0; u < 1024; u++) {
		analogWrite(pin, u);
		delay(1);
	}
}

void RGBTest() { // fade in and out of Red, Green, Blue

	analogWrite(RGB_RED, 1023); // R off
	analogWrite(RGB_GREEN, 1023); // G off
	analogWrite(RGB_BLUE, 1023); // B off

	RGBFade(RGB_RED); // R /G
	RGBFade(RGB_GREEN); // G /B
	RGBFade(RGB_BLUE); // B /R
}

void RGBInit(){

	#ifdef DEBUG
		DPRINT("RGB Init");
	#else
		pinMode(RGB_RED, OUTPUT);
		pinMode(RGB_GREEN, OUTPUT);
		pinMode(RGB_BLUE, OUTPUT);
	#endif

	RGBSetColor(255,255,255);

	RGBTest();
}

String RGBSet(int value) {
	String color;

	if(value < 25){
		color = "00796b";
		RGBSetColor(0, 121, 10); //0-25	// ok
		
	}else if(value < 50){
		color = "f9a825"; 
		RGBSetColor(249, 180, 37); //25-50//ok
		
	}else if(value < 75){
		color = "e65100";
		RGBSetColor(220, 93, 0);//50-75//ok
		
	}else if(value < 100){
		color = "dd2c00";
		RGBSetColor(255, 1, 0);//75-100//ok
		
	}else if(value < 500){
		color = "8c0084";
		RGBSetColor(204, 0, 132);//100-500//ok 
		
	}else{
		color = "52004E";
		RGBSetColor(120, 0, 120);//500 >//ok

	}
	return color;
}

