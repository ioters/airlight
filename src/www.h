const char MAIN_page[] PROGMEM = R"=====(
<html>
  <head>
    <title>My Sensors</title>
    <style>
      body{color: #061046;}.onoff{border: 1px solid #8e8e8e; padding: 0.2em 0.8em;}table.tab{margin: 1%; border-spacing: unset;}table.tab caption{background: #bad49f; font-size: 1.6em; padding: 0.1em; border: 1px solid #999;}table.tab th{text-align: left; width: 10%; white-space: nowrap; border-left: 1px solid #999; padding-right: 0.2em;}table.tab tr:last-child>*{border-bottom: 1px solid #999;}table.tab tr>:last-child{border-right: 1px solid #999;}table.tab tr:nth-child(odd)>*{background: #e8f2f9;}table.tab tr>*{padding: 0.5em;}button{background: #e8e8e8; border: 1px solid #929292; padding: 0.2em 0.6em; cursor: pointer;}#info_tab{clear: both;}
      #message { background: #e8f2f9; color: #7c2fab; text-align: center; font-size: 1em; position: absolute; right: 1em; top:33%; }
      h1 { text-align: center; margin: 0; background: #9785b9; color: #f3e6c8; }
      .section_label { background: #e8dff9 !important; }
      .section { width: 45%; float: left; min-width: 470px; }
	  .long{width:250px}
	  #current_value_label{padding:3px 5px}
      #easy_name_h { color: #ebe3fb; }
    </style>
  </head>
  <body>

  <h1>AirLight - <span id="easy_name_h"></span></h1>
  <div class="section">
    <table class="tab">
    <caption>Status</caption>
    <tr>
      <th>Current value:</th>
      <td><span id="current_value_label">N/A</span></td>
    </tr>
    <tr>
      <th>WiFi Signal:</th>
      <td><span id="ssi_label">N/A</span>%</td>
    </tr>
    <tr>
      <th>Memory:</th>
      <td id="heap_label">N/A</td>
    </tr>
    <tr>
      <th>Last update:</th>
      <td id="last_update_label" class="dt">N/A</td>
    </tr>
    <tr>
      <th>Firmware Version:</th>
      <td id="firmware_version_label" class="dt">N/A</td>
    </tr>
    <tr>
      <th>
        <button onclick="getStatus(true)">Refresh</button>
      </th>
      <td>
		 <!--
         <button onclick="setTest(1)">Test 1</button> 
         <button onclick="setTest(25)">Test 25</button> 
         <button onclick="setTest(50)">Test 50</button> 
         <button onclick="setTest(75)">Test 75</button> 
         <button onclick="setTest(100)">Test 100</button> 
         <button onclick="setTest(500)">Test 500</button> 
         <button onclick="setTest(1000)">Test 1000</button> 
         <br>
         <input type="text" style="width:100px" id="test_red"/>
         <input type="text" style="width:100px" id="test_green"/>
         <input type="text" style="width:100px" id="test_blue"/>
         <button onclick="setTestColor()">Test</button> 
		 -->
      </td>
    </tr>
  </table>

  <table class="tab" id="info_tab">
    <caption>Info</caption>
    <tbody id="info_block"></tbody>
  </table>
</div>

<div class="section">
  <form action="/" onsubmit="return saveSettings(this);">
    <table class="tab">
      <caption>Settings</caption>
      <tr>
        <th>Easy name:</th>
        <td colspan="2">
          <input type="text" id="easy_name_label" class="input" name="easy_name">
        </td>
      </tr>
      <tr><th colspan="3" class="section_label">Remote Data</th></tr>
      <tr>
        <th>Airtube:</th>
        <td>
          <input type="radio" name="data_type" value="airtube"/>
		  <input type="text" id="data_location_label" class="input long" name="data_location">
        </td>
        <td>
          
        </td>
      </tr>
	  <tr>
        <th>Local sernsor:</th>
        <td>
          <input type="radio" name="data_type" value="local"/>
		  <input type="text" id="data_url_label" class="input long" name="data_url">
        </td>
        <td>
          <button type="button" OnClick="dataUpdate();">Get data now</button>
        </td>
      </tr>
      <tr><th colspan="3" class="section_label">Remote Firmware Update</th></tr>
      <tr>
        <th>Path:</th>
        <td>
          <input type="text" id="update_url_label" class="input long" name="update_url">
        </td>
        <td>
          <button type="button" OnClick="firmwareUpdate();">Update now</button>
        </td>
      </tr>
      <tr>
      <th colspan="3" style="text-align: center; position:relative;">
        <button type="button" style="float: left;" OnClick="restoreDefaults();">Restore Defaults</button>
        <button type="submit">Save</button><span id="message"></span>
      </th>
      </tr>
    </table>
  </form>
</div>

<script type="text/javascript">

    var set_inputs_flag = true;
    var stop_status_recu = false;
    var easy_name = false;

    function p(name, step){
      change(name, step, 1);
    }

    function m(name, step){
      change(name, step, -1);
    }

    var TimeOuts = {};

    function change(name, step, mode){
      if(TimeOuts[name]) clearTimeout(TimeOuts[name]);

      var el = g(name + '_label');
      var value = Number(el.innerHTML);
      if(!isNumeric(value)){
        value = 20;
      }
      value = Math.round((value + (step * mode)) * 10) / 10;
      el.innerHTML = value;
      var post = {};
      post[name] = value;

      TimeOuts[name] = setTimeout(function(){
        save(post, function(){
          delete TimeOuts[name];
        });
      }, 600);
    }

    function setv(name){
      var post = {};
      post[name] = g(name + '_label').value;
      save(post);
    }

    function save(post, cb){
      call('save', post, function (data){
        if(cb) cb();
        g('message').innerHTML = 'Saved';
        setTimeout(function (){
          g('message').innerHTML = '';
        }, 3000);
      });
    }

    function getStatus(set_inputs, recu_time){

      set_inputs_flag = !!set_inputs;

      var complete_cb = false;
      if(recu_time){
        complete_cb = function(){
          setTimeout(function(){
            if(!stop_status_recu){
              getStatus(set_inputs, recu_time);
            }
          }, recu_time);
        }
      }

      call('status', null, function (data){
        data.ssi = 100 + data.ssi;
        place(data);
        g('last_update_label').innerHTML = new Date().toLocaleString();
      }, complete_cb);

    }

    function getInfo(){
      call('info', null, function (data){
        var html = '';
        console.log(data);
        for(var d in data){
          html += '<tr><th>' + d + ':</th><td>' + data[d] + '</td></tr>';
        }
        g('info_block').innerHTML = html;
      })
    }

    function restartCallback(){
      g('last_update_label').innerHTML = 'Restarting...';
      stopPeridicUpdate();
      setTimeout(function(){
        initialRequests();
        setTimeout(function(){
          startPeridicUpdate();
        }, 5000);
      }, 7000);
    }

    function rebootNode(){
      var cc = confirm("You are about to set the node to sleep mode. Are you sure?");
      if(cc){
        call('sleep', null, function (data){
          restartCallback();
        })
      }

    }

    function restoreDefaults(){
      var cc = confirm("You are about to restore all settings to defaults. Are you sure?");
      if(cc){
        call('restore_defaults', null, function (data){
          restartCallback();
        })
      }
    }

    function place(data){
      console.log(data);

      for(var d in data){
        var value = data[d];
        var label = d + ( d=='temp_sensor_type' ? '_' + value  : '') + '_label';
        var el = g(label);
        if(el == null || el === document.activeElement || (el.nodeName == 'INPUT' && !set_inputs_flag) || TimeOuts[d] !== undefined) continue;

        if(el.type == 'radio'){
          el.checked = true;
          continue;
        }

        value = (value == '') ? 'N/A' : value;
        if(el.className == 'onoff'){
          el.style = 'background-color:#' + ( value == 1 ? '75c715' : 'f33e3e');
          value = (value == 1) ? 'ON' : 'OFF';
        }
        if(el.nodeName == 'INPUT'){
          el.value = value;
        }else{
          el.innerHTML = value;
        }
      }
      g('current_value_label').style = 'background-color:#'+data.current_color;
      if(easy_name !== data.easy_name){
          easy_name = data.easy_name;
          document.title = 'AirLight ' + easy_name;
          g('easy_name_h').innerHTML = easy_name;
      }

    }

    function call(url, post, success_cb, complete_cb){
      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function (){
        if(this.readyState == 4){
          if(this.status == 200){
            if(success_cb){
              success_cb(JSON.parse(this.responseText))
            }
          } else {
            // Error responce
          }
          if(complete_cb){
            complete_cb.call(this);
          }
        }
      };
      if(post){
        var params = [];
        for(var p in post){
          params.push(p + '=' + post[p]);
        }
        url += '?' + params.join('&');
      }
      xhttp.open("GET", "/" + url, true);
      xhttp.send();
    }

    function isNumeric(n){
      return !isNaN(parseFloat(n)) && isFinite(n);
    }

    function g(id){
      return document.getElementById(id);
    }

    function saveSettings(form){
      var els = form.querySelectorAll('.input');
      var post = {};
      for(var i = 0; i < els.length; i++){
        if(els[i].type == 'radio' && !els[i].checked) continue;

        var name = els[i].name;
        var value = els[i].value;
        post[name] = value;
      }
      console.log(post);
      save(post);

      setTimeout(function(){ getStatus(true); }, 1000);
      return false;
    }

    function dataUpdate(){
      stopPeridicUpdate();
      call('data_update', null, function (data){
        setTimeout(function(){ getStatus(true); }, 1000);
      });
   
    }

    function firmwareUpdate(){
      var cc = confirm("The device will try to update it's firmware. Are you sure you want to continue?");
      if(cc){
        stopPeridicUpdate();
        call('firmware_update', null, function (data){
          // TODO countdown some time and restart the page
        });
      }
    }
    function setTest(value){
      var post = {}
      post['test_value'] = value; 
      call('test_value', post, function (data){
        setTimeout(function(){ getStatus(true); }, 1000);
      });
    }
    
    function setTestColor(){
      var post = {}
      post['red'] = g("test_red").value; 
      post['green'] = g("test_green").value;
      post['blue'] = g("test_blue").value;
       
      call('test_color', post, function (data){
        setTimeout(function(){ getStatus(true); }, 1000);
      });
    }

    function initialRequests(){
      getInfo();
      getStatus(true);
    }

    function startPeridicUpdate(){
      stop_status_recu = false;
      getStatus(false, 5000);
    }
    function stopPeridicUpdate(){
      stop_status_recu = true;
    }

    initialRequests();
    setTimeout(function(){
      startPeridicUpdate();
    }, 30000);

  </script>
  </body>
</html>
)=====";