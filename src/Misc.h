#ifndef _IOTERS_MISC_H_
#define _IOTERS_MISC_H_


void restartESP () {
  DPRINTLN("Restating ESP on http request demand...");
  delay(1000);
  ESP.reset();
  delay(1000);
}

#endif