#include <ArduinoJson.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

HTTPClient http;

String WebClientConnect(){


	DPRINT(F("Connecting to ..."));
	DPRINTLN(SETTINGS.data_url);
	
  if (http.begin(String(SETTINGS.data_url))) {
    
	  DPRINTLN(F("Connected!"));

	  int httpCode = http.GET();

	  // httpCode will be negative on error
	  if (httpCode > 0) {
	    // HTTP header has been send and Server response header has been handled
	    
	    // file found at server
	    if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
	      
	      String payload = http.getString();
	      //DPRINT(payload);
				return payload;
			  
			}else{
				 DPRINT("[HTTP] response code, error:");
     		DPRINTLN(http.errorToString(httpCode).c_str());
			}

    }else{
     DPRINT("[HTTP] GET... failed, error:");
     DPRINTLN(http.errorToString(httpCode).c_str());
    }
	 
	}else{
		  DPRINTLN(F("Connection failed!!!"));
  	
	}
	return "";
}

int WebClientReadData(){

	String payload = WebClientConnect();

	const size_t bufferSize = JSON_ARRAY_SIZE(9) + 10*JSON_OBJECT_SIZE(2) + 400;
	DynamicJsonBuffer jsonBuffer(bufferSize);
	JsonObject& root = jsonBuffer.parseObject(payload);

	JsonArray& sensordatavalues = root["sensordatavalues"];
  return (int)sensordatavalues[0]["value"]; // "4.47"
	
}
