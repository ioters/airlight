#include <FS.h>                   //this needs to be first, or it all crashes and burns...

#include <SPI.h>

#include <Arduino.h>


#include "Debug.h"

const char* firmware_version = "AirLight#0.6";

extern "C" {
#include <user_interface.h>
}

#define DEBUG

#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <WiFiManager.h>

#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson

int current_value = 0;
String current_color = "";
rst_info *myResetInfo;

#include "Misc.h"
#include "Settings.h"
#include "WebClient.h"
#include "RGB.h"
#include "WiFi.h"
#include "WebServer.h"
#include "OTAUpdate.h"

unsigned long lastConnectionTime = 0;           // last time you connected to the server, in milliseconds
const unsigned long postingInterval = 60*1000;  


void setup() {
  

  #ifdef DEBUG
    Serial.begin(115200);
  #else
    digitalWrite(3, HIGH);
  #endif

  delay(4000); // slow down so we really can see the reason!!
  myResetInfo = ESP.getResetInfoPtr();
  
  DPRINTLN("Start setup");

  delay(100);
  
  SettingsInit();
  delay(100);
  
  WiFiInit();
  delay(100);
  
  WebServerInit();
  delay(100);
  
  RGBInit();
  
  
}

void loop() {

  checkWifiConnection();
  
  if (!lastConnectionTime || millis() - lastConnectionTime > postingInterval) {
    lastConnectionTime = millis();
    current_value = WebClientReadData();
    DPRINT("Value:");
    DPRINTLN(current_value);
    if(current_value){
      current_color = RGBSet(current_value);
    }
  }

  handleClient();
}

