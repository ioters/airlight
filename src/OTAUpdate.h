#ifndef _IOTERS_OTAUPDATE_H_
#define _IOTERS_OTAUPDATE_H_

#include <ESP8266httpUpdate.h>

#include "Debug.h"
#include "Settings.h"
#include "WiFi.h"

void checkAutoUpdate() {

  if(getWiFiStatus()){

    DPRINTLN(F("Check for firmware update"));
    DPRINT(SETTINGS.update_url);
    DPRINTLN(firmware_version);
    
    ESPhttpUpdate.rebootOnUpdate(true);
    
    t_httpUpdate_return ret = ESPhttpUpdate.update(SETTINGS.update_url, firmware_version);

    switch(ret) {
      case HTTP_UPDATE_FAILED:
        DPRINTLN("[update] Update failed.");
        DPRINTLN(ESPhttpUpdate.getLastErrorString());
        break;
      case HTTP_UPDATE_NO_UPDATES:
        DPRINTLN("[update] Update no Update.");
        break;
      case HTTP_UPDATE_OK:
        DPRINTLN("[update] Update ok."); // may not called we reboot the ESP
        break;
    }
  }

}

#endif