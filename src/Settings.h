#ifndef _SETTINGS_H_
#define _SETTINGS_H_

#include <ESP_EEPROM.h>
#include "Debug.h"

char CONFIG_VERSION[6] = "VER06";
char DEFAULT_EASY_NAME[30] = "AirLight";
char DEFAULT_DATA_URL[150] = "http://192.168.25.34/data.json";
char DEFAULT_UPDATE_URL[150] = "http://192.168.25.24/otaupdate/";

struct  MyEEPROMStruct 
{
  char config_version[6]  = "VER06";
  char easy_name[30]= "AirLight";
  char update_url[150] = "http://192.168.25.24/otaupdate/";
  char data_url[150] = "http://192.168.25.34/data.json";

} SETTINGS, DEFAULT_SETTINGS;



// Save settings to FS
void SettingsSave () {
  DPRINT("saving config:");
  EEPROM.put(0, SETTINGS);
  boolean ok = EEPROM.commitReset();
  DPRINTLN((ok) ? "OK" : "Failed");
}

// Read configuration from FS json
int SettingsLoad () {

  DPRINTLN("Reading EEPROM...");
  if(EEPROM.percentUsed()>=0) {
    DPRINTLN("Has records in eeprom");
    EEPROM.get(0, SETTINGS);

    if (SETTINGS.config_version == CONFIG_VERSION){
      DPRINTLN("Load config");
      DPRINTLN(SETTINGS.easy_name);
      
      return 1; // return 1 if config loaded 
    }
  }
  DPRINTLN("Default config");
   
  SettingsSave();
  return 0; // return 0 if config NOT loaded
}

void SettingsInit(){
  DPRINTLN("EEPROM Init");
  EEPROM.begin(sizeof(MyEEPROMStruct));
  SettingsLoad();
}

void SettingsReset () {
  DPRINTLN("Reset config");
  EEPROM.wipe();
}

#endif