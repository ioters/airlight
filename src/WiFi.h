#ifndef _IOTERS_WIFI_H_
#define _IOTERS_WIFI_H_

#include "Debug.h"

WiFiManager wifiManager;

byte wifi_status = 0;
long last_wifi_check = 0;
unsigned long network_reconect_time      = 30000;

int connectWifi () {
  DPRINTLN(F("Connecting as wifi client..."));

  if (WiFi.SSID()) {
    DPRINTLN("Using last saved values, should be faster");
    //trying to fix connection in progress hanging
    ETS_UART_INTR_DISABLE();
    wifi_station_disconnect();
    ETS_UART_INTR_ENABLE();

    WiFi.begin();
  } else {
    DPRINTLN("No saved credentials");
  }

  int connRes = WiFi.waitForConnectResult();
  DPRINTLN ("Connection result: ");
  DPRINTLN ( connRes );

  return connRes;
}

void setWifiStatus () {
  if (WiFi.status() == WL_CONNECTED) {
    if(wifi_status != 1) {
      DPRINTLN(F("Wifi is connected!"));
      DPRINT("Local IP: ");
      DPRINTLN(WiFi.localIP());
      //digitalWrite(pin_led_blue,  HIGH);
      wifi_status = 1;
    }
  } else {
    if(wifi_status != 0) {
      DPRINTLN(F("Wifi connection FAILD!"));
      //digitalWrite(pin_led_blue,  LOW);
      wifi_status = 0;
    }
  }
}

int getWiFiStatus (){
    return wifi_status;
}

void checkWifiConnection () {

  if (!last_wifi_check || millis() - last_wifi_check > network_reconect_time )  {
    DPRINTLN(F("Wifi check"));
    if (WiFi.status() != WL_CONNECTED) {
      DPRINTLN(F("Wifi connecting..."));
      WiFi.mode(WIFI_STA);
      connectWifi();
    }
    setWifiStatus();
    last_wifi_check = millis();
  }
}

void WiFiInit(){

  String host_name = WiFi.hostname();
  host_name.toCharArray(SETTINGS.easy_name, 80);

  //wifiManager.resetSettings();
  wifiManager.setTimeout(180);
  String ap_ssid = "AirLight-" + String(ESP.getChipId());
  if(!wifiManager.autoConnect(ap_ssid.c_str())) {
      DPRINTLN(F("Failed to connect and hit timeout"));
      restartESP();
  }

}

#endif